This doc captures failure modes (hardware, software, network, boat), their risks, and their status. As we design the system, we can evaluate ideas against possible failures.

**Please add content freely.**  It's much better to capture noise than to lose signal!

# Bilge pump alert system

## Incoming water rate > max bilge pump rate
Highest priority alarm as $`\frac{dy}{dt} < 0`$.

Knowing only the *pump cycle count* wouldn't convey this condition.

## Failed to receive/log heartbeat for substantial amount of time
## Receiving heartbeats but recent history has heartbeat id# gap
## Float switch defective, stuck on
## Float switch defective, stuck off
## Pump physically/electrically defective
## Battery failure or general electrical system failure
## Hoses leaking
## Strainer or other components clogged
