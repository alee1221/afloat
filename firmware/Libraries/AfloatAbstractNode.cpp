/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */


#include "AfloatAbstractNode.h"

#include "Libraries/mesh_ack.h"
#include "Libraries/utilities.h"

#include "gitcommit.h"


#define BATTERY_UPDATE_INTERVAL_S 60

#define LOW_BATTERY_VOLTAGE 3.4f
#define CRITICAL_BATTERY_VOLTAGE 3.0f
#define DEPLETED_BATTERY_VOLTAGE 2.8f


/**
 * @brief AfloatAbstractNode::AfloatAbstractNode Constructor for sample rate nodes
 */
AfloatAbstractNode::AfloatAbstractNode(String sensorName, String dataFieldName, String description, uint32_t sampleInterval_ms, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks)
{
   // Set instance variables
   this->sensorName = sensorName;
   this->dataFieldName = dataFieldName;
   this->description = description;
   this->sampleInterval_ms = sampleInterval_ms;
   this->connectionInterval_s = connectionInterval_s;
   this->maxNumPendingAcks = maxNumPendingAcks;

   // Assign the memory
   ackList = new ackTuple_t[maxNumPendingAcks];

   // Iterate over all ACK tuples and set the default ACKed state to `true`
   for (int i=0; i<maxNumPendingAcks; i++) {
      std::get<TUPLE_ACKED>(ackList[i]) = true;
   }
}


/**
 * @brief AfloatAbstractNode::AfloatAbstractNode Constructor for interrupt-based nodes
 */
AfloatAbstractNode::AfloatAbstractNode(String sensorName, String dataFieldName, String description, std::initializer_list<pin_t> wakeUpPins, std::initializer_list<InterruptMode> edgeTriggerModes, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks)
{
   // Set instance variables
   this->sensorName = sensorName;
   this->dataFieldName = dataFieldName;
   this->description = description;
   this->connectionInterval_s = connectionInterval_s;
   this->maxNumPendingAcks = maxNumPendingAcks;

   this->wakeUpPins = wakeUpPins;
   this->edgeTriggerModes = edgeTriggerModes;

   this->sampleInterval_ms = 0;

   // Assign the memory
   ackList = new ackTuple_t[maxNumPendingAcks];

   // Iterate over all ACK tuples and set the default ACKed state to `true`
   for (int i=0; i<maxNumPendingAcks; i++) {
      std::get<TUPLE_ACKED>(ackList[i]) = true;
   }
}


/**
 * @brief AfloatAbstractNode::AfloatAbstractNode Copy constructor
 * @param obj Class instance to be copied
 */
AfloatAbstractNode::AfloatAbstractNode(const AfloatAbstractNode &obj)
{
   // Assign the memory
   ackList = new ackTuple_t[maxNumPendingAcks];

   // Iterate over the list and copy the tuples
   for (int i=0; i<maxNumPendingAcks; i++) {
      ackList[i] = obj.ackList[i];
   }
}


/**
 * @brief AfloatAbstractNode::~AfloatAbstractNode Destructor
 */
AfloatAbstractNode::~AfloatAbstractNode() {
   // Delete new'd object
   delete[] ackList;
}


/**
 * @brief AfloatAbstractNode::identityMsgProcessor Processes messages directed to this node
 * @param event The published topic
 * @param jsonMsgPacket The published data
 * @return
 */
bool AfloatAbstractNode::identityMsgProcessor(const char *event, const char *jsonMsgPacket)
{

   // If the sender is asking for a provisioning statement...
   if (strcmp(jsonMsgPacket, "whoami") == 0) {
      // ... send one!
      mustSendProvisioning = true;

      return true;
   } else if (strncmp(jsonMsgPacket, "ACK", 3) == 0) {  // The sender is acking a message
      int32_t ackedID = receiveAck(jsonMsgPacket[3]);

      // Check if the ACK ID is valid
      if (ackedID >= 0) {
         // Loop over all the ACK packets
         for (int i=0; i<maxNumPendingAcks; i++) {
            // If this ID is in the list, set it to false
            if (ackedID == std::get<TUPLE_ACK_ID>(ackList[i])) {
//               Serial.println(String(ackedID) + " removed from list at index " + String(i));
               std::get<TUPLE_ACKED>(ackList[i]) = true;

               return true;
            }
         }
      } else {
         Serial.println(String("[MESH][WARNING]") + String(ackedID) + "not found in list.");
      }
   }

   return false;
}


/**
 * @brief printAckTuple Helper function to examine the array of ACK tuples
 * @param ackElement the ACK tuple to be printed.
 */
void AfloatAbstractNode::printAckTuple(ackTuple_t ackElement)
{
   bool ack;
   time_t  lastXmitTime;
   uint32_t numberOfRetries;
   uint8_t  ackId;
   payload_t payload;

   // Do a left-hand assignment of the elements in the ACK tuple.
   std::tie(ack, ackId, lastXmitTime, numberOfRetries, payload) = ackElement;

   Serial.println("ACK: " + String(ack) + "   ackId: " + String(ackId)
                  + "   lastXmitTime: " + String(lastXmitTime)
                  + "   numberOfRetries: " + String(numberOfRetries));
}


/**
 * @brief sleepUntil Sleep until a given UTC time, in [s]
 * @param untilUtc_s UTC time (in seconds) at which the processor should wake up.
 */
void AfloatAbstractNode::sleepUntil(time_t untilUtc_s)
{
   // Get the current time
   time_t currentUtc_s = Time.now();

   // Ensure that the wake time is indeed in the future
   if (untilUtc_s > currentUtc_s) {
      // Compute the amount of time to sleep
      time_t sleepTime = untilUtc_s - currentUtc_s;

      System.sleep(wakeUpPins, edgeTriggerModes, sleepTime);
   } else {
      // Do nothing and bail out.
      Serial.println("Unable to sleep, inputs invalid: " + String(untilUtc_s) + String(" / ") + String(Time.now()));
      return;
   }
}


/**
 * @brief AfloatAbstractNode::updateBatteryState Checks the battery and publishes info to cloud. If the battery
 *        voltage is getting low, it also increases the sleep time as a means to save energy.
 */
void AfloatAbstractNode::updateBatteryState()
{
  // Periodically check battery.
  static time_t batteryUpdateTime_s = Time.now();
  if (batteryUpdateTime_s <= Time.now()) {
#if (PLATFORM_ID == PLATFORM_XENON || PLATFORM_ARGON)  // Xenon and Argon can only measure the battery voltage. This is a somewhat inaccurate indicator of remaining battery life.
     float batteryVoltage = analogRead(BATT) * 0.0011224;
     bool publishRet = Particle.publish(sensorName + String("/battery"), String(batteryVoltage), PRIVATE);

     // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and
     // the sleep might occur before the publish is complete.
     if (publishRet == true) {
        Serial.println("Successfully published battery report to cloud.");
     }

     // Advance next update time. If the battery is too low then be more aggressive about
     // trying to sleep longer.
     if (batteryVoltage < DEPLETED_BATTERY_VOLTAGE) {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S * 8;
     } else if (batteryVoltage < CRITICAL_BATTERY_VOLTAGE) {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S  * 4;
     } else if (batteryVoltage < LOW_BATTERY_VOLTAGE) {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S * 2;
     } else {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S;
     }
#elif  // The Electron and Boron have a fuel-gauge IC. This yields highly accurate measurements of remaining battery life.
     float stateOfCharge = Particle.getSoC();
     bool publishRet = Particle.publish(sensorName + String("/battery"), String(stateOfCharge), PRIVATE);

     // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and
     // the sleep might occur before the publish is complete.
     if (publishRet == true) {
        Serial.println("Successfully published battery report to cloud.");
     }

     // Advance next update time. If the state of charge is too low then be more aggressive about
     // trying to sleep longer.
     if (stateOfCharge < 10) {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S * 8;
     } else if (stateOfCharge < 20) {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S  * 4;
     } else if (stateOfCharge < 40) {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S * 2;
     } else {
        batteryUpdateTime_s += BATTERY_UPDATE_INTERVAL_S;
     }

#endif
  }
}


/**
 * @brief publishProvisioningMessage Publishes the node's provisioning message
 */
void AfloatAbstractNode::publishProvisioningMessage()
{
   // Create provisioning message
   const size_t capacity = JSON_ARRAY_SIZE(2) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(6);
   DynamicJsonDocument doc(capacity);

   doc["deviceName"] = sensorName;
   doc["dataFieldName"] = dataFieldName;
   doc["description"] = description;
   doc["sampleInterval"] = String(sampleInterval_ms);
   doc["connectionInterval"] = String(connectionInterval_s);

   JsonObject firmware = doc.createNestedObject("firmware");
   firmware["date"] = __DATE__;  // `__DATE__` is auto-generated by the compiler
   firmware["time"] = __TIME__;  // `__TIME__` is auto-generated by the compiler
   firmware["branch"] = GIT_BRANCH_NAME;  // `GIT_BRANCH_NAME` is auto-generated by a python script
   firmware["hash"] = GIT_COMMIT_HASH;  // `GIT_COMMIT_HASH` is auto-generated by a python script

   // Convert document to string
   char output[255];  // 255 is the max payload which can be sent with `Mesh.publish()`, C.f. https://docs.particle.io/reference/device-os/firmware/argon/#publish-
   /* serializeMsgPack(doc, output); */
   serializeJson(doc, output);

   Serial.println(String("[Provisioning] \t\t") + String(output));

   // Publish provisioning payload to cloud (not the mesh).
   bool isSuccessful = Particle.publish("sensor_provisioning", output, PRIVATE);

   // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might occur before the publish is
   // complete.
   if (!isSuccessful) {
      Serial.println("Unable to publish provisioning message to cloud");
   }
}


/**
 * @brief AfloatAbstractNode::sendData Publishes data to the mesh
 * @param sensorPacket the packet to be sent
 * @param payloadData Original data in the packet, which gets put into ackList upon a successful publish
 * @param numberOfRetries The number of times this packet has been published. This is stored in ackList
 */
void AfloatAbstractNode::sendData(DynamicJsonDocument &sensorPacket, const payload_t &payloadData, const uint32_t numberOfRetries)
{
   MeshPacketWithAck meshPacket(255);  // 255 is the max payload which can be sent with `Mesh.publish()`, C.f. https://docs.particle.io/reference/device-os/firmware/argon/#publish-

   //Serialize the JSON and send it to the mesh/cloud
   /* serializeMsgPack(sensorPacket, (char *)meshPacket.packet->data, meshPacket.size); */
   serializeJson(sensorPacket, (char *)meshPacket.packet->data, meshPacket.size);

   uint8_t packetID;
   int32_t meshReturn = publishWithAck(sensorName, meshPacket, &packetID);

#if 1
   // =====================
   Particle.publish(sensorName, (char *)meshPacket.packet->data, PRIVATE);
   // =====================
#endif

   if (meshReturn != 0) {
      // get here if event publish did not work
      Serial.println("Mesh.publish() not successful? Returns error code: " + String(meshReturn));
   } else {
      // Iterate over all the elements in ackList
      for (int i=0; i<maxNumPendingAcks; i++) {
         bool isACKed = std::get<TUPLE_ACKED>(ackList[i]);

         // If this element has been acked, then we can overwrite it
         if (isACKed == true) {
            // Get the current UTC time
            time_t tmpTime = Time.now();

            // Populate a fresh tuple
            ackList[i] = ackTuple_t(false, packetID, tmpTime, numberOfRetries, payloadData);

            break;
         }
      }
   }
}


/**
 * @brief AfloatAbstractNode::beginLoop Routine to be run at the beginning of the main loop
 * @param sleepState Returns the new sleep state
 * @param shouldSleepImmediately Indicates if the device should sleep immediately
 */
void AfloatAbstractNode::beginLoop(enum SLEEP_STATE *sleepState, bool *shouldSleepImmediately)
{

   // Get the mesh up and running
   if (Mesh.ready() == false) {
      Serial.println("[MESH] not ready, attempting to connect...");

      // Try to reconnect
      Mesh.connect();

      // Wait until mesh is ready
      const int32_t meshConnectionTimeout_ms = 2500;
      waitFor(Mesh.ready, meshConnectionTimeout_ms);

      // If the mesh is still not reconnected, go to sleep
      if (Mesh.ready() == false) {
         Serial.println("[MESH] Failed to connect.");

         // Disconnect from the mesh
         Mesh.disconnect();
         // Wait an arbitrary 100ms as lead time for the Mesh commands to fully propagate through hardware
         delay_nonblocking(100);

         // Power cycle the mesh unit (in hopes it fixes things!)
         Mesh.off();
         // Wait an arbitrary 100ms as lead time for the Mesh commands to fully propagate through hardware
         delay_nonblocking(100);

         Mesh.on();
         // Wait an arbitrary 100ms as lead time for the Mesh commands to fully propagate through hardware
         delay_nonblocking(100);

         // Prep for going to sleep
         *sleepState = SLEEP_TILL_SYNC_TIME;

         *shouldSleepImmediately = true;
      }
   }
}


/**
 * @brief AfloatAbstractNode::performNetworkHousekeeping Runs at the end of the loop, checks for ACKed packets and responds to any pending requests
 * @param sleepState Returns the new sleep state
 * @param shouldDelaySleep Indicates if the device should delay going to sleep in order to give the mesh time to finish propagating events
 */
void AfloatAbstractNode::performNetworkHousekeeping(enum SLEEP_STATE *sleepState, bool *shouldDelaySleep)
{
   // Check if we should send provisioning
   if (mustSendProvisioning) {
      publishProvisioningMessage();
      mustSendProvisioning = false;
   }

   // Check if there are timed out packets which should be resent
   for (int i=0; i<maxNumPendingAcks; i++) {
      bool isACKed = std::get<TUPLE_ACKED>(ackList[i]);
      if (isACKed == false) {
         time_t lastTransmitTime = std::get<TUPLE_LAST_XMIT_TIME>(ackList[i]);
         int32_t ellapsedTime = Time.now() - lastTransmitTime;

         // If the ACK has timed out
         if (ellapsedTime >= ackTimeout_s) {
            Serial.print("[MESH] Ellapsed ACK: \t");
            printAckTuple(ackList[i]);

            // Abandon prior attempt
            haltAck(std::get<TUPLE_ACK_ID>(ackList[i]));

            std::get<TUPLE_ACKED>(ackList[i]) = true;

            uint32_t numberOfRetries = std::get<TUPLE_NUMBER_OF_RETRIES>(ackList[i]);

            // If we haven't tried too many times, then try again
            if (numberOfRetries <= maxNumberOfRetries) {
               // Extract data from tuple
               payload_t payload_old = std::get<TUPLE_PAYLOAD>(ackList[i]);

               // Resend
               republishData(payload_old, numberOfRetries);

               // Increment retry counter
               std::get<TUPLE_NUMBER_OF_RETRIES>(ackList[i]) += 1;
            } else {
               Serial.println(String("[Mesh] Exceeded number of retries for ") + String(lastTransmitTime) + String(", ") + String(" timed out after ") + String(numberOfRetries) + " retries, abandoning.");
            }

         }
      }
   }

   //================================
   // ALL DONE, PREP FOR NEXT WAKE UP
   //================================

   // Loop over all members in the ACK list, in order to figure out if the node can go to sleep
   for (int i=0; i<maxNumPendingAcks; i++) {
      /* printAckTuple(ackList[i]); */

      bool isACKed = std::get<TUPLE_ACKED>(ackList[i]);

      // Check if any ACKs are pending
      if (isACKed == false) {
         Serial.print("[MESH] Unacked: \t");
         printAckTuple(ackList[i]);

         static enum SLEEP_STATE oldSleepState = SLEEP_UNDETERMINED;

         uint32_t numberOfRetries = std::get<TUPLE_NUMBER_OF_RETRIES>(ackList[i]);

         // Check if the number of retries is so high that we just need to go to sleep for a
         // while to try to resync with the mesh.
         if (numberOfRetries > maxNumberOfRetries) {
            Serial.println("[MESH][WARNING]: max number of retries exceeded.");

            // Check if the sleepState isn't already SLEEP_TILL_SYNC_TIME
            if (*sleepState != SLEEP_TILL_SYNC_TIME) {
               // Back up the sleep state
               oldSleepState = *sleepState;
               *sleepState = SLEEP_TILL_SYNC_TIME;
            }

            return;
         } else if (oldSleepState != SLEEP_UNDETERMINED) {
            Serial.println("[MESH]: returning sleepState to old value:" + String(oldSleepState));

            // If oldSleepState is holding backup information, then restore sleepState
            *sleepState = oldSleepState;

            // Wipe the old sleep state backup variable
            oldSleepState = SLEEP_UNDETERMINED;
         }

         // If we've made it this far, the delay sleep for a cycle
         *shouldDelaySleep = true;
      }
   }
}


/**
 * @brief AfloatAbstractNode::handleSleep Ingests the sleep state and determines proper action
 * @param sleepState Sleep state enum
 */
void AfloatAbstractNode::handleSleep(enum SLEEP_STATE sleepState)
{
   // Check what kind of sleep we should go into
   switch (sleepState)  {
   case SLEEP_TILL_INTERRUPTED: {  // Sleep till interrupt fires
      bool publishRet = Particle.publish(sensorName + String("/sleep"), String("Sleep until: INT (") + Time.timeStr() + String(")"), PRIVATE);

      // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might activate before the publish() is
      // complete.
      if (publishRet == true) {
         Serial.println("Successfully published to the cloud before interrupt sleep");
      }

      System.sleep(wakeUpPins, edgeTriggerModes);
   } break;
   case SLEEP_TILL_SYNC_TIME: { // Sync sleep, which allows the mesh node to resync with the nexus. This will only be needed if the mesh publish wasn't successful after multiple retries.
      static bool isInitialPass = false;

      // Every time through this code path, toggle the variable.
      isInitialPass = !isInitialPass;

      Serial.println("Toggled isInitialPass: " + String(isInitialPass));

      // This causes sleepTillSync not to run on every other pass, which gives the
      // subsystem time to resolve any pending subscriptions. For instance, servicing a
      // full provisioning handshake takes some time after awaking from sleep, and so if
      // we don't wait before going to sleep we can miss the results from the provisioning
      // routine.
      if (isInitialPass == true) {
         Serial.println("[MESH] SLEEP_TILL_SYNC_TIME commanded, giving the mesh time to fully propagate all events");

         // 5 seconds is an arbitrary amount of time which seems long enough for any other
         // mesh actors to react and any Particle.publish() commands to fully propagate.
         delay_nonblocking(5000);

         return;
      }

      /* Determine when to wake up. */
      static time_t wakeUpUTC_s = Time.now();  // Initialize wake up rhythm to current time

      // Make sure our wakeup time is in the future
      do {
         wakeUpUTC_s += connectionInterval_s;
      } while(wakeUpUTC_s < Time.now());

      bool publishRet = Particle.publish(sensorName + String("/sleep"), String("Sleep (sync) until: ") + Time.timeStr(wakeUpUTC_s), PRIVATE);

      // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might activate before the publish() is
      // complete.
      if (publishRet == true) {
         Serial.println("Successfully published to the cloud before syncing sleep");
      }

      sleepUntil(wakeUpUTC_s);
   } break;
   case SLEEP_TILL_NEXT_SAMPLE: { // Sleep until the next sample period
      /* Determine when to wake up. */
      static time_t wakeUpUTC_s = Time.now();  // Initialize wake up rhythm to current time

      // Make sure our wakeup time is in the future
      do {
         wakeUpUTC_s += (uint32_t)(sampleInterval_ms / 1000.0f + 0.5f);
      } while(wakeUpUTC_s < Time.now());

      bool publishRet = Particle.publish(sensorName + String("/sleep"), String("Sleep (interval) until: ") + Time.timeStr(wakeUpUTC_s), PRIVATE);

      // NOTE: this test is necessary because otherwise Particle.publish() is a non-blocking function, and the sleep might activate before the publish() is
      // complete.
      if (publishRet == true) {
         Serial.println("Successfully published to the cloud before interval sleep");
      }

      sleepUntil(wakeUpUTC_s);

   } break;

   case SLEEP_UNDETERMINED:  // Don't sleep, we don't understand what we're supposed to do here
   case SLEEP_DONT_SLEEP:  // Don't sleep at all
      /* DO NOTHING */
      break;
   }
}
